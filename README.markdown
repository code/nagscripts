Nagscripts
==========

Some Nagios shell scripts to simplify commands. `man(1)` pages included.

Thanks
------

These were written on company time with my employer [Inspire Net][1], who have
generously allowed me to open source them.

License
-------

Copyright (c) [Tom Ryder][2]. Distributed under [MIT License][3].

[1]: https://www.inspire.net.nz/
[2]: https://sanctum.geek.nz/
[3]: https://opensource.org/licenses/MIT

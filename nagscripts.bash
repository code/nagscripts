# Source this file to get host and service completion for the Nagscript tools.
_nagios_objects() {
    local word=${COMP_WORDS[COMP_CWORD]}
    declare -a cmd
    case $word in
        */*)
            cmd=(nagios-data-search -s ^"${word#*/}")
            ;;
        *)
            compopt -o nospace
            cmd=(nagios-data-search ^"$word")
            ;;
    esac
    while read -r object ; do
        case $object in
            "$word"*) COMPREPLY[${#COMPREPLY[@]}]=$object ;;
        esac
    done < <("${cmd[@]}")
}
complete -F _nagios_objects -o default \
    nagios-acknowledge     \
    nagios-clear           \
    nagios-downstream-list \
    nagios-data-search     \
    nagios-downtime        \
    nagios-exists          \
    nagios-force-check
